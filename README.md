# 3D MR-STAT in the brain

Repository with example images from a proof-of-concept study regarding a new 3D MR-STAT sequence.  
Images are in compressed NIfTI (nii.gz) format, comprise the whole brain and have a spatial resolution of 1 x 1 x 1 mm3.  
Data corresponds to two different tests:
- benchmarking test (baseline vs proposed sequence) with data from 5 different volunteers
- repeatibility test with 4 repetitions of the protocol on one volunteer.

## Authors and acknowledgment

This repository belongs to the results presented in:  
_Time-efficient, high-resolution 3T whole-brain relaxometry using Cartesian 3D MR-STAT with CSF suppression_  
by Hongyan Liu, Edwin Versteeg PhD, Miha Fuderer, Oscar van der Heide PhD, Martin Bastiaan Schilder, Cornelis A.T. van den Berg PhD, and Alessandro Sbrizzi PhD
- Available on Arxiv: https://arxiv.org/abs/2403.15379
